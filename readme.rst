DESARROLLO
La prueba consiste en crear una interacción entre una interfaz gráfica (Web) y un API del
clima. La idea de la interfaz es poder mostrar el clima desde la ubicación de donde se está
accediendo o desde la ciudad que el usuario decida (INPUT). Adicionalmente, la interfaz gráfica
debe ser amigable y para ello se debe usar HTML5, CSS3 y Bootstrap.
Se obtendrán puntos adicionales si:
 Si implementa iconos de http://fontello.com/ o http://fontawesome.io/.
 Si utiliza el API de Google Maps. El usuario en vez de introducir la ciudad que desea, este
podrá clickear sobre el mapa de Google (API) y así mostrar el clima de donde hizo click.
Source (APPIs):
 Clima: https://openweathermap.org/current
 Google Maps: https://developers.google.com/maps/documentation/javascript/?hl=es
Nota: El API del Clima devuelve un JSON el cual usted debe utilizar para realizar la funcionalidad
antes explicada.