$(function () {
    initMap();
});

var map;
var geocoder;
var infowindow;

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 4.570868, lng: -74.29733299999998 },
        zoom: 7
    });

    geocoder = new google.maps.Geocoder();
    infowindow = new google.maps.InfoWindow();

    google.maps.event.addListener(map, 'click', function (event) {
        geocodeLatLng(event.latLng.lat(), event.latLng.lng());
        getWeather("https://api.openweathermap.org/data/2.5/weather?lat=" + event.latLng.lat() + "&" + "lon=" + event.latLng.lng() + "&appid=e943def9356b3c2f885b7fef57a55a51");
    });
}

function geocodeLatLng(latitud, longitud) {
    var latlng = { lat: latitud, lng: longitud };
    geocoder.geocode({ 'location': latlng }, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                map.setZoom(11);
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });
                infowindow.setContent(results[0].formatted_address);
                infowindow.open(map, marker);
            } else {
                window.alert('No se encontraron resultados');
            }
        } else {
            window.alert('Geocoder fallo debido a: ' + status);
        }
    });
}

function getWeather(url)
{
    //api.openweathermap.org/data/2.5/weather?lat=35&lon=139
    $.get(url, function(data){
        var ciudad = data.name;
        var lat = data.coord.lat;
        var lng = data.coord.lon;
        var temp = data.main.temp;
        var tempmax = data.main.temp_max;
        var tempmin = data.main.temp_min;
        var presion = data.main.pressure;
        var humedad = data.main.humidity;
        var vel_viento = data.main.temp;
        var estado_cielo = data.weather[0].main;
        var descripcion = data.weather[0].description;
        
        $('#myModal .modal-body').html(
            "<p><i class='fas fa-map-marked-alt'></i> " + ciudad + "</p>" +
            "<p><i class='fas fa-map-marker-alt'></i> " + lat + ", " + lng + "</p>" +
            "<p><i class='fas fa-cloud'></i> " + estado_cielo + "</p>" +
            "<p><i class='fas fa-list-ul'></i> " + descripcion + "</p>" +
            "<p><i class='fas fa-thermometer-empty'></i> " + temp + " K [Máx: " + tempmax + "K, Mín: " + tempmin + "K]</p>" +
            "<p><i class='fas fa-bolt'></i> " + presion + "</p>" +
            "<p><i class='fas fa-tint'></i> " + humedad + "</p>"                                      
        );
        
        $('#myModal').modal('show');
    }, "json");
}

$('#mapa').on('click', function(event){
    event.preventDefault();
    $('#form-clima').slideUp('slow', function(){$(this).addClass('ocultar')})
	$('#map').slideDown('slow', function(){$(this).removeClass('ocultar')});
});

$('#input').on('click', function(event){
    event.preventDefault();
    $('#map').slideUp('slow', function(){$(this).addClass('ocultar')})
    $('#form-clima').slideDown('slow', function(){$(this).removeClass('ocultar')});
    
    $('#buscar-clima').on('click', function(event){
        event.preventDefault();
        var ciudad = $('#ciudad').val();
        var patron = /([A-Za-zñÑáéíóúÁÉÍÓÚ]+(\,\s)+[A-Za-zñÑáéíóúÁÉÍÓÚ]+)|([A-Za-z]+(\,\s)+[A-Z]+)/;
        if( patron.test(ciudad) ) 
        {
            getWeather("https://api.openweathermap.org/data/2.5/weather?q=" + ciudad + "&appid=e943def9356b3c2f885b7fef57a55a51");
        }else{
            ciudad = ciudad + ", Colombia";
            getWeather("https://api.openweathermap.org/data/2.5/weather?q=" + ciudad + "&appid=e943def9356b3c2f885b7fef57a55a51");            
        }
    })
})